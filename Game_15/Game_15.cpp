#include "Game_15.h"


int abs(int x) {
	return x > 0 ? x : -x;
}

Game_15::Game_15() {
	for (int i = 0, n = 1; i < 4; ++i) {
		for (int j = 0; j < 4; ++j, ++n) {
			gameField[j][i] = n != 16 ? n : 0;
		}
	}
	zeroX = 3;
	zeroY = 3;
}
void Game_15::InitGame() {
	//���� ����� - �������� �������
	//����� ������ ���������� ������ � gameField
	//��� ���� ���� ������ ����������
}

int Game_15::GetValue(int x, int y) {
	return gameField[x][y];
}
Coord Game_15::GetCoord(int value) {
	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			if (gameField[j][i] == value)
				return Coord(j, i);
		}
	}
	return Coord(-1, -1);
}
Coord Game_15::GetZeroCoord() {
	return Coord(zeroX, zeroY);
}

bool Game_15::Go(int value) {
	Coord valueCoord = GetCoord(value);
	if (abs(valueCoord.x - zeroX) + abs(valueCoord.y - zeroY) != 1)
	{
		return false;
	}
	gameField[zeroX][zeroY] = value;
	gameField[zeroX = valueCoord.x][zeroY = valueCoord.y] = 0;
	return true;
}

bool Game_15::IsWin() {
	///���� ����� - �������� �������
	//������ ���������� TRUE ��� ��������
	return false;
}
