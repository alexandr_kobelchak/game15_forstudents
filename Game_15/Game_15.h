#pragma once

int abs(int);

struct Coord {
	int x;
	int y;
	Coord(int x, int y) :x(x), y(y) {
	}
};


class Game_15
{
private:
	int gameField[4][4];
	int zeroX;
	int zeroY;
public:
	Game_15();
	void InitGame();
	int GetValue(int x, int y);
	Coord GetCoord(int value);
	Coord GetZeroCoord();
	bool Go(int value);
	bool IsWin();
};

