#include "observer.h"
#include "stdafx.h"

BOOL CALLBACK Observer::DialogProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	static Observer obs(hWnd);
	switch (Message) {
		HANDLE_MSG(hWnd, WM_COMMAND, obs.OnCommand);
		HANDLE_MSG(hWnd, WM_INITDIALOG, obs.OnInitDialog);
	}
	/* All other messages (a lot of them) are processed using default procedures */
	return FALSE;
}

void Observer::OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify)
{
	switch (id) {
	//case IDOK:
	case IDCANCEL:
		EndDialog(hWnd, id);
		break;
	}
	if (id > 10000 && id < 10016) {
		int value = id - 10000;
		if (m_Game.Go(value)) {
			Coord coord = m_Game.GetCoord(value);
			MoveWindow(hwndCtl,
				m_btnPadding + (m_btnMargin + m_btnSize) * coord.x,
				m_btnPadding + (m_btnMargin + m_btnSize) * coord.y,
				m_btnSize, m_btnSize,
				true);
			if (m_Game.IsWin()) {
				MessageBox(hWnd, L"You are WIN !!!", L"", MB_OK);
			}
		}
		else {
			MessageBox(hWnd, L"Error", L"", MB_OK);
		}
	}
}

BOOL Observer::OnInitDialog(HWND hWnd, HWND hWndFocus, LPARAM lParam) {

	int gameSize = 2 * m_btnPadding + (m_btnMargin + m_btnSize) * 4;
	//SendMessage(hWnd, WM_SIZE, SIZE_RESTORED, (gameSize + 100) << 16 | gameSize);

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			
			int value=m_Game.GetValue(j, i);
			if (!value) continue;

			std::wstringstream wss;			
			wss << value;
			CreateWindow(L"button",
				wss.str().c_str(),
				WS_CHILD | WS_VISIBLE | BS_CENTER | BS_VCENTER,
				m_btnPadding + (m_btnMargin + m_btnSize) * j,
				m_btnPadding + (m_btnMargin + m_btnSize) * i,
				m_btnSize, m_btnSize,
				m_hWnd,
				(HMENU)(10000 + value),
				GetModuleHandle(NULL),
				(LPVOID) value);
		}
	}

	return TRUE;
}


