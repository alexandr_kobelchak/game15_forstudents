#pragma once
#include "stdafx.h"
#include "Game_15.h"

class Observer
{
private:
	HWND m_hWnd;

	int m_btnSize = 100;
	int m_btnMargin = 10;
	int m_btnPadding = 20;
	Game_15 m_Game;


public:
	Observer(HWND hWnd) :m_hWnd(hWnd) {
		m_Game.InitGame();
	}
	/* This is where all the input to the window goes to */
	static BOOL CALLBACK DialogProc(HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam);
	void OnCommand(HWND hWnd, int id, HWND hwndCtl, UINT codeNotify);
	BOOL OnInitDialog(HWND hwnd, HWND hwndFocus, LPARAM lParam);
};

